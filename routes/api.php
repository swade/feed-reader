<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(
    ['middleware' => ['auth:api']],
    function () {
        Route::delete('/multi/{id}/{feedId}', 'Api\MultisController@remove');
        Route::post('/multi/{id}/add-feed', 'Api\MultisController@addFeed');
        Route::put('/multi/{id}', 'Api\MultisController@update');
        Route::delete('/multi/{id}', 'Api\MultisController@destroy');
        Route::delete('/feed/{id}', 'Api\FeedController@unsubscribe');
    }
);

