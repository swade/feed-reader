<?php

Auth::routes(['verify' => true]);
Route::redirect('/', '/all', 301);
Route::get(
    '/logout',
    [
        'middleware' => ['auth', 'verified'],
        'uses' => '\App\Http\Controllers\Auth\LoginController@logout'
    ]
);

Route::group(
    ['middleware' => ['auth', 'verified']],
    function () {
        Route::get('/me', ['uses' => 'UserController@profile', 'as' => 'user.profile']);
        Route::post('/me', ['uses' => 'UserController@update', 'as' => 'user.profile.update']);
    }
);


Route::group(
    [
        'middleware' => ['auth', 'verified'],
        'prefix' => 'f'
    ],
    function () {
        Route::get('/manage', ['uses' => 'FeedController@manage', 'as' => 'feeds.manage']);
        Route::get('/all', ['uses' => 'FeedController@index', 'as' => 'feed.index']);
        Route::get('/subscribe', ['uses' => 'FeedController@create', 'as' => 'feed.create']);
        Route::post('/', ['uses' => 'FeedController@store', 'as' => 'feed.store']);
        Route::get('/{id}-{host?}', ['uses' => 'FeedController@show', 'as' => 'feed.show']);
        Route::delete('/{id}', ['uses' => 'FeedController@unsubscribe', 'as' => 'feed.unsubscribe']);
    }
);

Route::get('/link/{slug}', ['uses' => 'LinkController@show', 'as' => 'link.show']);

Route::group(
    [
        'middleware' => ['auth', 'verified'],
        'prefix' => 'm'
    ],
    function () {
        Route::get('/new', ['uses' => 'MultisController@create', 'as' => 'multi.create']);
        Route::get('/{slug}', ['uses' => 'MultisController@show', 'as' => 'multi.show']);
        Route::post('/', ['uses' => 'MultisController@store', 'as' => 'multi.store']);
    }
);
