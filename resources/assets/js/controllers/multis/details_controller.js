import { Controller } from 'stimulus';
import axios from 'axios';

export default class extends Controller {
  static targets = [
    'description',
    'descriptionInput',
    'name',
    'nameInput',
    'form'
  ];

  edit() {
    this.toggleForm(true);
  }

  delete() {
    if (!confirm('Are you sure you want to delete this multifeed?')) {
      return;
    }

    axios.delete(`/api/multi/${this.id}`)
      .then((response) => {
        window.location.href = response.headers.location;
      })
  }

  save() {
    const data = {
      name: this.getInput('name'),
      description: this.getInput('description')
    };

    axios.put(`/api/multi/${this.id}`, data)
      .then((response) => {
        this.name = data.name;
        this.description = data.description;
        this.toggleForm();
      })
      .catch((response) => {
        // TODO handle errors
        this.resetForm();
      })
  }

  cancel() {
    this.toggleForm();
    this.resetForm();
  }

  resetForm() {
    this.nameInputTarget.value = this.name;
    this.descriptionInputTarget.value = this.description;
  }

  toggleForm(show = false) {
    this.formTarget.classList.toggle('hidden', !show);
  }

  getInput(field) {
    switch (field) {
      case 'name' :
        return this.nameInputTarget.value;
      case 'description' :
        return this.descriptionInputTarget.value;
      default :
        return '';
    }
  }

  // GETTERS AND SETTERS

  get id() {
    return this.data.get('id');
  }

  get name() {
    return this.nameTarget.textContent;
  }

  set name(text) {
    this.nameTarget.textContent = text;
  }

  get description() {
    return this.descriptionTarget.textContent;
  }

  set description(text) {
    this.descriptionTarget.textContent = text;
  }
}
