import { ApplicationController } from "../application-controller";
import axios from 'axios';
import $ from 'jquery';

export default class extends ApplicationController {
  static targets = ['feedSearch', 'feed', 'feedList'];

  initialize() {
    this.rebuildAutoComplete();

    $(this.feedSearchTarget).autocomplete({
      lookup: this.getFeedSuggestions.bind(this),
      onSelect: (suggestion) => {
        this.feedSearchTarget.value = suggestion.value;
        this.feedSearchTarget.dataset.feedId = suggestion.data.id;
      }
    })
  }

  getFeedSuggestions(query, done) {
    const suggestions = this.autoCompleteList.map((feed) => {
        return {
          value: feed.host,
          data: {id: feed.id}
        };
      })
      .filter(suggestion => {
        return $.Autocomplete.defaults.lookupFilter(suggestion, query, query.toLowerCase())
      });

    done({suggestions});
  }

  addFeed() {
    //this.feedTarget
    const feedIdOrUrl = this.feedSearchTarget.dataset.feedId || this.feedSearchTarget.value;
    axios({
      method: 'post',
      url: `/api/multi/${this.id}/add-feed`,
      data: {feedIdOrUrl},
      headers: {'Accept': 'text/html'},
    })
      .then(this.onAddSuccess.bind(this))
      .catch((error) => {
        // TODO handle errors
        console.log(error);
      })
  }

  onAddSuccess(response) {
    this.insertFeed(response.data);
    this.refreshFeed();
    this.resetInput();
    this.rebuildAutoComplete();
  }

  insertFeed(html) {
    const $el = $(html);

    if (this.feedTargets.length === 0) {
      $(this.feedListTarget).append($el);
      return;
    }

    $(this.feedTargets).each(function () {
      if ($(this).find('a').text() > $el.find('a').text()) {
        $el.insertBefore($(this));
      }
    });
  }

  resetInput() {
    this.feedSearchTarget.value = '';
    this.feedSearchTarget.dataset.feedId = '';
  }

  removeFeed(event) {
    const parent = event.target.parentElement;
    const feedId = parent.dataset.feedId;

    axios.delete(`/api/multi/${this.id}/${feedId}`)
      .then((response) => {
        parent.parentNode.removeChild(parent);
        this.refreshFeed();
        this.rebuildAutoComplete();
      })
      .catch((error) => {
        // TODO handle errors
        console.log(error);
      });
  }

  refreshFeed() {
    const feedController = this.getControllerByIdentifier('feed');
    feedController.reload();
  }

  rebuildAutoComplete() {
    const feeds = appData.feeds || [];
    const ids = $(this.feedTargets).map(function() {
        return parseInt(this.dataset.feedId);
      })
      .get();

    this.autoCompleteList = feeds.filter((feed) => {
      return $.inArray(feed.id, ids) === -1;
    })
  }

  get id() {
    return this.data.get('id');
  }
}
