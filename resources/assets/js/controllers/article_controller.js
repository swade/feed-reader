import { ApplicationController } from "./application-controller";

export default class extends ApplicationController {
  static targets = ['link'];

  viewArticle(event) {
    if(event.target !== event.currentTarget) return;
    if (event.ctrlKey || event.metaKey) return;
    event.preventDefault();
    event.stopPropagation();

    window.location.href = this.data.get('url');
  }
}
