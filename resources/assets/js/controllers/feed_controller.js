import { Controller } from 'stimulus';
import $ from 'jquery';

export default class extends Controller {
  reload() {
    $(this.element).load(`${this.data.get('url')} #feed-container`);
  }
}
