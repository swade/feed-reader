import { ApplicationController } from "./application-controller";
import axios from 'axios';
import $ from 'jquery';

export default class extends ApplicationController {
  static targets = ['link'];

  viewFeed(event) {
    if(event.target !== event.currentTarget) return;
    if (event.ctrlKey || event.metaKey) return;
    event.preventDefault();
    event.stopPropagation();

    window.location.href = this.data.get('url');
  }

  unsubscribe(event) {
    if (!confirm(`Are you sure you want to unsubscribe from ${this.data.get('host')}?`)) return;

    axios.delete(`/api/feed/${this.data.get('id')}`)
      .then((response) => {
        $(this.element).remove();
      })
      .catch((error) => {
        // TODO handle errors
        console.log(error);
      });
  }
}
