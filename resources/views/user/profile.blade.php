@extends('layouts.master')

@section('body')

    <div class="container-fluid mt-3">
        <div class="row justify-content-center">
            <main class="col-4">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="card">
                    <div class="card-body">
                        <form action="{!! route('user.profile.update') !!}" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Name <small>(required)</small></label>
                                <input type="text" name="name" class="form-control" value="{!! $user->name !!}" />
                            </div>
                            <div class="form-group">
                                <label>Email <small>(required)</small></label>
                                <input type="email" name="email" class="form-control" value="{!! $user->email !!}" />
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" name="password" class="form-control" placeholder="password"/>
                                <br>
                                <input type="password" name="password_confirmation" class="form-control" placeholder="verify password" />
                            </div>
                            <button type="submit" class="btn btn-primary border-primary-accent border-bottom border-top-0 border-right-0 border-left-0">save</button>
                        </form>
                    </div>
                </div>
            </main>
        </div>
    </div>

@endsection
