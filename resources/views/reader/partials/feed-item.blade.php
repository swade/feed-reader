<div
        class="d-flex flex-row p-2-5 feed-item {!! !$loop->last ? 'border-bottom' : '' !!}"
        data-controller="manage"
        data-manage-id="{!! $feed->id !!}"
        data-manage-host="{!! $feed->host() !!}"
        data-manage-url="{!! route('feed.show', ['id' => $feed->id, 'host' => $feed->host()]) !!}"
        data-manage-unsubscribe-url="{!! route('feed.unsubscribe', ['id' => $feed->id]) !!}"
>
    @if($feed->image)
        <div class="article-image mr-3">
            <img src="{!! $feed->image !!}" class="rounded"/>
        </div>
    @endif
    <div class="item h-100 w-100">
        <header class="feed-item-header">
            <a
                    href="{!! $feed->link !!}"
                    class="text-primary article-title"
                    target="_blank"
                    data-action="click->manage#viewFeed"
            >
                {{ $feed->title }}
            </a>
        </header>
        <div class="sub-heading">
            <small>{!! $feed->description !!}</small>
        </div>
    </div>
    <div class="align-top">
        <button
                type="button"
                class="btn btn-danger btn-sm border-0"
                data-action="click->manage#unsubscribe"
        >&times; unsubscribe</button>
    </div>
</div>
