<form action="{!! route('feed.store') !!}" method="post">
    @csrf
    <?php echo $formExtra ?? ''; ?>
    <div class="form-group">
        <label>{!! $formLabel ?? 'Feed URL' !!}</label>
        <input type="url" name="url" class="form-control" placeholder="https://">
    </div>
    <button type="submit" class="btn btn-primary border-primary-accent border-bottom border-top-0 border-right-0 border-left-0">subscribe</button>
</form>
