@extends('layouts.master')

@section('body')

    <div class="container-fluid mt-3">
        <div class="row justify-content-center">
            <main class="col-4">
                <div class="card">
                    <div class="card-body">
                        @include('reader.partials.add-form')
                    </div>
                </div>
            </main>
        </div>
    </div>

@endsection
