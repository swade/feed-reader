@extends('layouts.sidebar')

@section('body')

    <div id="feed-container"
         class="bg-white rounded border"
         data-controller="feed"
         data-feed-url="{!! url()->current() !!}"
    >
        <section class="feed">
            @each('reader.article', $links, 'link')
        </section>
    </div>

    @include('partials.pagination', ['results' => $links])

@endsection

@section('sidebar')

    @include('partials.create-feed-btn')

@endsection
