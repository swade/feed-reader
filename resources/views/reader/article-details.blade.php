@extends('layouts.sidebar')

@section('body')

    <div class="article-details row">
        @if(url()->previous() !== url()->current())
        <div class="col-1">
            <a href="{!! url()->previous() !!}" class="btn btn-light btn-sm border bg-white">&laquo; back</a>
        </div>
        @endif
        <article class="article-full col">
            <div class="card">
                <div class="card-body">
                    <h2 class="mb-1">{!! $link->title !!}</h2>
                    <div class="sub-heading mb-3 pb-1 border-bottom">
                        <small>
                            Published on {!! $link->pub_date->format('F j, Y') !!}
                            | <a href="{!! $link->link !!}" target="_blank" class="text-secondary">view original</a>
                            | <a href="#" class="bookmark text-secondary">share</a>
                            | <a href="#" class="bookmark text-secondary">save</a>
                            | <a href="#" class="bookmark text-secondary">mark as unread</a>
                        </small>
                    </div>
                    <section class="description">
                        {!! $link->content !!}
                    </section>
                </div>
            </div>
        </article>
    </div>

@endsection

@section('sidebar')

    @include('partials.create-feed-btn')

@endsection
