<article
    class="d-flex flex-row p-2-5 feed-article border-bottom"
    data-controller="article"
    data-action="click->article#viewArticle"
    data-article-url="{!! route('link.show', ['slug' => $link->slug()]) !!}"
>
    @if($link->getImage())
    <div class="article-image mr-3">
        <img src="{!! $link->getImage() !!}" class="rounded"/>
    </div>
    @endif
    <div class="item h-100 w-100">
        <header class="article-header">
            <a
                href="{!! $link->link !!}"
                class="text-primary article-title"
                target="_blank"
                data-target="article.link"
                data-action="article#viewArticle"
            >
                {{ $link->title }}
            </a>
            @foreach($link->feed->multis as $multi)
            <!--<small><a class="badge badge-light" href="{!! route('multi.show', ['id' => $multi->id]) !!}">{!! $multi->name !!}</a></small>-->
            @endforeach
        </header>
        <div class="sub-heading">
            <small>
                {!! $link->timeAgo() !!}
                @if(\Route::current()->getName() !== 'feed.show')
                 on <a href="{!! route('feed.show', ['id' => $link->feed->id, 'host' => $link->feed->host()]) !!}" class="text-primary" data-url="{!! $link->feed->site_url !!}">{{ $link->feed->host() }}</a>
                @endif
                | <a href="#" class="bookmark text-secondary">share</a>
                | <a href="#" class="bookmark text-secondary">save</a>
                | <a href="#" class="bookmark text-secondary">mark as read</a>
            </small>
        </div>
    </div>
</article>
