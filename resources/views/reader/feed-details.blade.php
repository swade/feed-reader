@extends('reader.main')

@section('sidebar')
    <div class="feed-details">
        @if($feed->image)
        <img src="{!! $feed->image !!}" class="rounded mx-auto d-block mb-3 mw-100" alt="{!! $feed->title !!}">
        @endif

        <div class="card mb-3">
            <div class="card-body">
                <h3>{!! $feed->title !!}</h3>

                <form
                    action="{!! route('feed.unsubscribe', ['id' => $feed->id]) !!}"
                    method="post"
                    class="mt-3 d-block"
                    onsubmit="return confirm('Are you sure you want to unsubscribe from {!! $feed->host() !!}?');"
                >
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-danger btn-sm border-0">&times; unsubscribe</button>
                </form>

                <p class="feed-description mt-3">{!! $feed->description !!}</p>
            </div>
        </div>
    </div>

    @parent
@endsection
