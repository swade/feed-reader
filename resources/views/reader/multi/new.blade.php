@extends('layouts.master')

@section('body')

    <div class="container-fluid mt-3">
        <div class="row justify-content-center">
            <main class="col-4">
                <form action="{!! route('multi.store') !!}" method="post">
                    @csrf
                    <div class="form-group bg-light p-2 rounded">
                        <label>Name <small>(required)</small></label>
                        <input type="text" name="name" class="form-control" />
                    </div>
                    <div class="form-group bg-light p-2 rounded">
                        <label>Description</label>
                        <textarea name="description" class="form-control"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary border-primary-accent border-bottom border-top-0 border-right-0 border-left-0">create</button>
                </form>
            </main>
        </div>
    </div>

@endsection
