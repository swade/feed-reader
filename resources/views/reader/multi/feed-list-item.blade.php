<?php $host = $feed->host(); ?>
<li class="multi" data-feed-id="{!! $feed->id !!}" data-target="multis--feed-list.feed">
    <a href="{!! route('feed.show', ['id' => $feed->id, 'host' => $host]) !!}" class="text-primary-accent">
        {!! $host !!}
    </a>
    <span class="remove text-muted font-weight-bold" data-action="click->multis--feed-list#removeFeed">&times;</span>
</li>
