@extends('reader.main')

@section('sidebar')
    <div class="card mb-3">
        <div class="card-body">
            <section class="multi-meta" data-controller="multis--details" data-multis--details-id="{!! $multi->id !!}">
                <h3 class="" data-target="multis--details.name">{!! $multi->name !!}</h3>
                <div data-target="multis--details.description">{!! $multi->description !!}</div>
                <div class="meta-actions">
                    <a href="#" class="text-muted mr-1" data-action="click->multis--details#edit">edit</a>
                    <a href="#" class="text-muted" data-action="click->multis--details#delete">delete</a>
                </div>
                <form method="post" class="mt-3 clearfix hidden" data-target="multis--details.form">
                    <div class="form-group">
                        <input type="text" class="form-control form-control-sm" placeholder="Multi Name" value="{!! $multi->name !!}" data-target="multis--details.nameInput">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control form-control-sm" placeholder="Description" data-target="multis--details.descriptionInput">{!! $multi->description !!}</textarea>
                    </div>
                    <button type="button" class="btn btn-light btn-sm border-bottom float-right" data-action="click->multis--details#save">save</button>
                    <button type="button" class="btn btn-link btn-sm float-right mr-1" data-action="click->multis--details#cancel">cancel</button>
                </form>
            </section>

            <section class="multi-feeds mt-3" data-controller="multis--feed-list" data-multis--feed-list-id="{!! $multi->id !!}">
                <div class="font-weight-bold">Feeds</div>
                <div class="input-group input-group-sm mt-1 mb-1">
                    <input type="text" class="form-control" placeholder="add feed" data-target="multis--feed-list.feedSearch" data-feed-id="" />
                    <div class="input-group-append">
                        <button class="btn btn-primary-accent" data-action="click->multis--feed-list#addFeed">+</button>
                    </div>
                </div>
                <ul class="multi-feeds-list list-inline mb-0" data-target="multis--feed-list.feedList">
                    @each('reader.multi.feed-list-item', $multi->feeds->sort(), 'feed')
                </ul>
            </section>
        </div>
    </div>

    @parent

@endsection
