@extends('layouts.master')

@section('body')

    <div class="container-fluid mt-3">
        <div class="row justify-content-center">
            <main class="col-6">
                <div class="card">
                    <div class="card-body">
                        @foreach($feeds as $feed)
                            @include('reader.partials.feed-item')
                        @endforeach
                    </div>
                </div>
            </main>
            <aside class="col-4">
                <div class="card">
                    <div class="card-body">
                        <?php
                            $formExtra = '<input type="hidden" name="_redirect" value="' . url()->current() . '" />';
                        ?>
                        @include('reader.partials.add-form', ['formLabel' => 'Add New Feed', 'formExtra' => $formExtra])
                    </div>
                </div>
            </aside>
        </div>
    </div>

@endsection
