@if($results->total() > $results->perPage())
<nav class="pt-3 pb-3">
    <ul class="pagination pagination-sm mb-0">
        @if($results->currentPage() > 1)
        <li class="page-item"><a class="page-link border-primary-accent" href="{!! $results->previousPageUrl() !!}">&laquo; Previous</a></li>
        @endif
        @if($results->hasMorePages())
        <li class="page-item"><a class="page-link border-primary-accent" href="{!! $results->nextPageUrl() !!}">Next &raquo;</a></li>
        @endif
    </ul>
</nav>
@endif
