<div class="dropdown right input">
    <a class="btn btn-link dropdown-toggle text-white" href="#" role="button" id="dropdown-add-feed" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Add Feed
    </a>

    <div class="dropdown-menu border-0 px-2" aria-labelledby="dropdown-add-feed">
        <div class="input-group input-group-sm">
            <input class="form-control" placeholder="url"/>
            <div class="input-group-append">
                <button class="btn btn-primary">save</button>
            </div>
        </div>
    </div>
</div>
