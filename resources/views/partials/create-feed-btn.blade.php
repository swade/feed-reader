@if(($asCard ?? true) === false)
    <a href="{!! route('feed.create') !!}" class="btn btn-primary-accent btn-block font-weight-bold text-white">New Subscription +</a>
@else
    <div class="card">
        <div class="card-body">
            <a href="{!! route('feed.create') !!}" class="btn btn-primary-accent btn-block font-weight-bold text-white">New Subscription +</a>
        </div>
    </div>
@endif
