<!doctype html>
<html>
<head>
    <title>Feed Reader</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="api-token" content="{{ auth()->user()->api_token }}">

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    @if(isset($appData))
    <script type="text/javascript">
        const appData = @json($appData);
    </script>
    @endif

    @stack('frontStack')
</head>
<body class="bg-light">
