<header id="site-header">
    <nav class="navbar navbar-dark navbar-expand-lg bg-primary">
        <a class="navbar-brand" href="{!! route('feed.index') !!}">Feed Reader</a>

        <div class="collapse navbar-collapse">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link text-white dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Feeds
                    </a>
                    <div class="dropdown-menu p-0 border-primary-accent">
                        @foreach ($feeds->sort() as $feed)
                            <a class="dropdown-item" href="{!! route('feed.show', ['id' => $feed->id, 'host' => $feed->host()]) !!}">{!! $feed->title !!}</a>
                        @endforeach
                        <div class="dropdown-divider m-0"></div>
                        <a class="dropdown-item" href="{!! route('feeds.manage') !!}">Manage Subscriptions</a>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link text-white dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Multis
                    </a>
                    <div class="dropdown-menu p-0 border-primary-accent">
                        @foreach ($multis->sort() as $multi)
                        <a class="dropdown-item" href="{!! route('multi.show', ['slug' => $multi->slug]) !!}">{!! $multi->name !!}</a>
                        @endforeach
                        <div class="dropdown-divider m-0"></div>
                        <a class="dropdown-item" href="{!! route('multi.create') !!}">New <span class="font-weight-bold">+</span></a>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item dropdown right">
                    <a href="#" class="nav-link text-white dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{!! $user->name !!}</a>
                    <div class="dropdown-menu p-0 border-primary-accent">
                        <a class="dropdown-item" href="{!! route('user.profile') !!}">Settings</a>
                        <a class="dropdown-item" href="{!! route('logout') !!}">Log out</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>

@if(session()->has('alert'))
    <?php $alert = session()->get('alert'); ?>
    @alert(['type' => $alert['type'] ?? 'primary'])
        {{ $alert['message'] ?? '' }}
    @endalert
@endif
