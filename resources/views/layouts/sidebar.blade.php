@include('partials.header')
    @include('partials.head')

    <div class="container-fluid mt-3">
        <div class="row">
            <main id="main" class="col">
                @yield('body')
            </main>

            <aside id="sidebar" class="col-3">
                @yield('sidebar')
            </aside>
        </div>
    </div>

@include('partials.footer')
