@include('partials.header')
    @include('partials.head')

    <div class="container-fluid mt-3">
        <div class="row">
            @yield('body')
        </div>
    </div>

@include('partials.footer')
