<?php

use App\Jobs\CreateFeed;
use App\User;
use GuzzleHttp\Psr7\Uri;
use Illuminate\Database\Seeder;

class FeedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user  = User::find(1);
        $feeds = [
            'https://feed.laravel-news.com/',
            'https://webdesignledger.com/feed/',
            'https://www.sitepoint.com/feed/',
//            'https://kiddingaroundgreenville.com/feed',
        ];

        foreach ($feeds as $feed) {
            CreateFeed::dispatchNow(
                new Uri($feed),
                $user
            );
        }

        CreateFeed::dispatchNow(
            new Uri('https://kiddingaroundgreenville.com/feed'),
            User::find(2)
        );
    }
}
