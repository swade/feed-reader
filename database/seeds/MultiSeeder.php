<?php

use App\Feed;
use App\Multi;
use Illuminate\Database\Seeder;

class MultiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $multi = Multi::create([
            'name' => 'Web Dev',
            'user_id' => 1
        ]);

        $feeds = Feed::find([1, 2]);

        $multi->feeds()->attach($feeds->pluck('id'));
    }
}
