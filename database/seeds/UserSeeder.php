<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'Steven Wade',
            'email' => 'stevenwadejr@gmail.com',
            'password' => Hash::make('password'),
            'email_verified_at' => new Carbon
        ]);

        \App\User::create([
            'name' => 'John Doe',
            'email' => 'jdoe@example.com',
            'password' => Hash::make('password'),
            'email_verified_at' => new Carbon
        ]);
    }
}
