<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Multi extends Model
{
    public $guarded = ['id'];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($multi) {
            $multi->slug = Str::slug($multi->name);
        });
    }

    public function feeds()
    {
        return $this->belongsToMany(Feed::class);
    }
}
