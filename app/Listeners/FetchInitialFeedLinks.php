<?php

namespace App\Listeners;

use App\Events\FeedCreated;
use App\Jobs\FetchFeedLinks;

class FetchInitialFeedLinks
{
    public function handle(FeedCreated $event)
    {
        \Log::debug('Fetch initial feed links');
        FetchFeedLinks::dispatch($event->feed);
    }
}
