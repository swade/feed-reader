<?php

namespace App;

use App\Events\FeedCreated;
use GuzzleHttp\Psr7\Uri;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use League\Uri\Components\Host;

class Feed extends Model
{
    use Notifiable;

    public $guarded = ['id'];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => FeedCreated::class,
    ];

    public function user()
    {
        return $this->belongsToMany(User::class);
    }

    public function links()
    {
        return $this->hasMany(Link::class);
    }

    public function multis()
    {
        return $this->belongsToMany(Multi::class);
    }

    public function host()
    {
        $host = (new Uri($this->site_url))->getHost();

        return (new Host($host))->getRegistrableDomain();
    }

    public function toArray()
    {
        return array_merge(
            parent::toArray(),
            [
                'host' => $this->host()
            ]
        );
    }

    public function scopeForUser(Builder $query, User $user)
    {
        return $query->whereHas('user', function ($query) use ($user) {
            $query->where('user_id', $user->id);
        });
    }

}
