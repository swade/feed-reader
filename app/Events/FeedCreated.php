<?php

namespace App\Events;

use App\Feed;
use Illuminate\Queue\SerializesModels;

class FeedCreated
{
    use SerializesModels;

    /**
     * @var Feed
     */
    public $feed;

    public function __construct(Feed $feed)
    {
        \Log::debug('Feed created');
        $this->feed = $feed;
    }
}
