<?php

namespace App\Jobs;

use App\Feed;
use App\User;
use GuzzleHttp\Psr7\Uri;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Zend\Feed\Reader\Feed\Atom;
use Zend\Feed\Reader\Feed\FeedInterface;
use Zend\Feed\Reader\Feed\Rss;
use Zend\Feed\Reader\Reader;

class CreateFeed implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Uri
     */
    protected $uri;

    /**
     * @var User
     */
    protected $user;

    public function __construct(Uri $uri, User $user)
    {
        $this->uri  = $uri;
        $this->user = $user;
    }

    public function handle()
    {
        $feed = Feed::where('feed_url', (string) $this->uri)->first();

        if (!$feed) {
            $rawFeed = Reader::import((string) $this->uri);
            $feed    = Feed::create([
                'title'           => $rawFeed->getTitle(),
                'description'     => $rawFeed->getDescription(),
                'feed_url'        => (string) $this->uri,
                'site_url'        => $rawFeed->getLink(),
                'image'           => $this->getImage($rawFeed),
                'last_build_date' => $rawFeed->getDateModified(),
            ]);
        }

        $feed->user()->detach($this->user->id);
        $feed->user()->attach($this->user->id);

        return $feed;
    }

    protected function getImage(FeedInterface $rawFeed): ?string
    {
        if (\method_exists($rawFeed, 'getImage') === false) {
            return null;
        }

        $image = $rawFeed->getImage();

        return $image['uri'] ?? null;
    }
}
