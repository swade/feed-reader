<?php

namespace App\Jobs;

use App\Feed;
use App\Link;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Zend\Feed\Reader\Entry\EntryInterface;
use Zend\Feed\Reader\Reader;

class FetchFeedLinks implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Feed
     */
    protected $feed;

    public function __construct(Feed $feed)
    {
        $this->feed = $feed;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::debug('Fetching links');
        $rawFeed = Reader::import($this->feed->feed_url);
        $links   = [];

        foreach ($rawFeed as $link) {
            /** @var $link EntryInterface */
            $links[] = [
                'title'       => \trim($link->getTitle()),
                'description' => \trim(
                    \strip_tags($link->getDescription())
                ),
                'content'     => \trim($link->getContent()),
                'image'       => $this->getImage($link),
                'link'        => $link->getLink(),
                'pub_date'    => $link->getDateModified(),
            ];
        }

        $this->feed->links()->createMany($links);
    }

    public function getImage(EntryInterface $entry): ?string
    {
        preg_match('/<img[^>]+>/i', $entry->getContent(), $results);

        foreach ($results as $result) {
            preg_match('/(src)=("[^"]*")/i', $result, $image);
            if (count($image) === 3) {
                return trim($image[2], '"\'');
            }
        }

        return null;
    }
}
