<?php declare(strict_types=1);

namespace App\Http\ViewComposers;

use App\Feed;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\View\View;

class FeedComposer
{
    /**
     * @var Guard
     */
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function compose(View $view)
    {
        $feeds = Feed::whereHas('user', function($query) {
                $query->where('user_id', $this->auth->user()->id);
            })
            ->get();

        $appData = $view->appData ?? [];
        $appData['feeds'] = $feeds->toArray();

        $view->appData = $appData;
    }
}
