<?php declare(strict_types=1);

namespace App\Http\ViewComposers;

use App\Feed;
use App\Multi;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\View\View;

class NavComposer
{
    /**
     * @var Guard
     */
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function compose(View $view)
    {
        $multis = Multi::where([
                'user_id' => $this->auth->user()->id
            ])
            ->get();

        $view->multis = $multis;

        $feeds = Feed::whereHas('user', function($query) {
                $query->where('user_id', $this->auth->user()->id);
            })
            ->get();

        $view->feeds = $feeds;

        $view->user = $this->auth->user();
    }
}
