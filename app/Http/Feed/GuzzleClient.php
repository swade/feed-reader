<?php declare(strict_types=1);

namespace App\Http\Feed;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use Zend\Feed\Reader\Http\ClientInterface as FeedReaderHttpClientInterface;
use Zend\Feed\Reader\Http\Psr7ResponseDecorator;

class GuzzleClient implements FeedReaderHttpClientInterface
{
    /**
     * @var Client|GuzzleClientInterface
     */
    protected $client;

    public function __construct(GuzzleClientInterface $client = null)
    {
        $this->client = $client ?? new Client;
    }

    public function get($uri)
    {
        return new Psr7ResponseDecorator(
            $this->client->request('GET', $uri, ['verify' => false])
        );
    }
}
