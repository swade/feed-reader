<?php

namespace App\Http\Controllers\Api;

use App\Feed;
use App\Jobs\CreateFeed;
use GuzzleHttp\Psr7\Uri;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class FeedController extends Controller
{
    public function store(Request $request)
    {
        $feed = CreateFeed::dispatchNow(new Uri($request->get('url')), $request->user());

        return response()->json(['id' => $feed->id]);
    }

    public function unsubscribe(Request $request, $id)
    {
        $request->user()->feeds()->detach($id);

        return response(null, 204);
    }
}
