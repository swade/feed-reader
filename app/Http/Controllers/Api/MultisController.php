<?php

namespace App\Http\Controllers\Api;

use App\Feed;
use App\Jobs\CreateFeed;
use App\Multi;
use GuzzleHttp\Psr7\Uri;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MultisController extends Controller
{
    public function remove(Request $request, int $id, int $feedId)
    {
        $multi = Multi::where([
            'user_id' => $request->user()->id,
        ])
            ->findOrFail($id);

        $multi->feeds()->detach($feedId);

        return response(null, 204);
    }

    public function addFeed(Request $request, int $id)
    {
        $feedIdOrUrl = $request->get('feedIdOrUrl');
        $multi       = Multi::where('user_id', $request->user()->id)
            ->findOrFail($id);

        if (\is_numeric($feedIdOrUrl)) {
            $feed = Feed::where('user_id', $request->user()->id)
                ->findOrFail($feedIdOrUrl);
        } else {
            $feed = CreateFeed::dispatchNow(new Uri($feedIdOrUrl), $request->user());
        }

        $multi->feeds()->attach($feed);

        if ($request->header('Accept') === 'text/html') {
            return view('reader.multi.feed-list-item', \compact('feed'));
        }

        return response()->json($feed);
    }

    public function update(Request $request, int $id)
    {
        $multi = Multi::where('user_id', $request->user()->id)
            ->findOrFail($id);

        $multi->update([
            'name' => $request->get('name'),
            'description' => $request->get('description')
        ]);

        return response(null, 200);
    }

    public function destroy(Request $request, int $id)
    {
        $multi = Multi::where('user_id', $request->user()->id)
            ->findOrFail($id);

        $multi->delete();

        return response(null, 204, ['Location' => \route('feed.index')]);
    }
}
