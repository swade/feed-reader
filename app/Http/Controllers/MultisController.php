<?php

namespace App\Http\Controllers;

use App\Multi;
use App\Link;
use Illuminate\Http\Request;

class MultisController extends Controller
{
    public function show(Request $request, string $slug)
    {
        $multi = Multi::with('feeds')
            ->where([
                'user_id' => $request->user()->id,
                'slug' => $slug
            ])
            ->firstOrFail();

        $links = Link::whereIn(
                'feed_id',
                $multi->feeds->pluck('id')
            )
            ->paginate(25);

        return view('reader.multi.main', \compact('multi', 'links'));
    }

    public function create()
    {
        return view('reader.multi.new');
    }

    public function store(Request $request)
    {
        $multi = Multi::create([
            'user_id' => $request->user()->id,
            'name' => $request->get('name'),
            'description' => $request->get('description')
        ]);

        return \redirect()->route('multi.show', ['slug' => $multi->slug]);
    }
}
