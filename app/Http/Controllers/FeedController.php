<?php

namespace App\Http\Controllers;

use App\Feed;
use App\Jobs\CreateFeed;
use App\Link;
use GuzzleHttp\Psr7\Uri;
use Illuminate\Http\Request;

class FeedController extends Controller
{
    public function index(Request $request)
    {
        $links = Link::forUser($request->user())
            ->orderBy('pub_date', 'DESC')
            ->paginate(25);

        return view('reader.main', \compact('links'));
    }

    public function manage(Request $request)
    {
        $feeds = $request->user()->feeds;

        return view('reader.manage', \compact('feeds'));
    }

    public function create()
    {
        return view('reader.add');
    }

    public function store(Request $request)
    {
        CreateFeed::dispatchNow(new Uri($request->get('url')), $request->user());

        $request->session()->flash('alert', ['type' => 'success', 'message' => 'Subscription added!']);

        if ($request->has('_redirect')) {
            return \redirect($request->get('_redirect'));
        }

        return redirect()->route('feed.index');
    }

    public function show(Request $request, int $id, ?string $host = null)
    {
        $feed = $request->user()->feeds()->findOrFail($id);

        $links = $feed->links()
            ->orderBy('pub_date', 'DESC')
            ->paginate(25);

        return view('reader.feed-details', \compact('feed', 'links'));
    }

    public function unsubscribe(Request $request, int $id)
    {
        $feed = $request->user()->feeds()->findOrFail($id);

        if ($feed->detach($request->user())) {
            $request->session()->flash('alert', ['type' => 'success', 'message' => $feed->host() . ' unsubscribed']);
        }

        return redirect()->route('feed.index');
    }
}
