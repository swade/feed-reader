<?php

namespace App\Http\Controllers;

use App\Link;
use Illuminate\Http\Request;

class LinkController extends Controller
{
    public function show(Request $request, string $slug)
    {
        $link = Link::forUser($request->user())->findOrFail((int) $slug);

        return view('reader.article-details', \compact('link'));
    }
}
