<?php

namespace App\Providers;

use App\Http\Feed\GuzzleClient;
use Illuminate\Support\ServiceProvider;
use Zend\Feed\Reader\Reader;

class ZendFeedProvider extends ServiceProvider
{
    public function boot()
    {
        Reader::setHttpClient(new GuzzleClient);
    }

    public function register()
    {
        //
    }
}
