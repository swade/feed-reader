<?php

namespace App\Providers;

use App\Http\ViewComposers\FeedComposer;
use App\Http\ViewComposers\NavComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('reader.multi.main', FeedComposer::class);
        View::composer('partials.head', NavComposer::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
