<?php

namespace App;

use GuzzleHttp\Psr7\Uri;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Westsworld\TimeAgo;

class Link extends Model
{
    public $guarded = ['id'];

    protected $casts = [
        'pub_date' => 'datetime',
    ];

    public function feed()
    {
        return $this->belongsTo(Feed::class);
    }

    public function timeAgo()
    {
        return (new TimeAgo)->inWords($this->pub_date);
    }

    public function getImage(): ?string
    {
        return $this->image
            ?? $this->feed->image
            ?? null;
    }

    public function slug(): string
    {
        return Str::slug($this->id . ' ' . $this->title);
    }

    public function scopeForUser(Builder $query, User $user)
    {
        return $query->with('feed')
            ->whereHas('feed.user', function($query) use ($user) {
                $query->where('user_id', $user->id);
            });
    }

    public function toArray()
    {
        return array_merge(
            parent::toArray(),
            [
                'timeAgo' => $this->timeAgo(),
                'slug'    => $this->slug(),
            ]
        );
    }
}
